#include "tcpdialog.h"
#include "ui_tcpdialog.h"

TcpDialog::TcpDialog(QWidget *parent,TcpPort * tcpport) : QDialog(parent), ui(new Ui::TcpDialog)
{
    ui->setupUi(this);
    this->tcpport = tcpport;

    connect(this->ui->connectButton,SIGNAL(clicked()),tcpport,SLOT(openPort()));
    connect(this->ui->disconnectButton,SIGNAL(clicked()),tcpport,SLOT(closePort()));

    connect(this->tcpport,SIGNAL(setUiStatus(QString)),this,SLOT(setUiStatus(QString)));
}

TcpDialog::~TcpDialog()
{
    delete ui;
}

void TcpDialog::setUiStatus(QString str)
{
    this->ui->connection_label->setText(str);
}
