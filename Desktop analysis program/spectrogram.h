#ifndef SPECTROGRAM_H
#define SPECTROGRAM_H

#include <QObject>
#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPixmap>
#include <QPainter>
#include <QDebug>
#include <QScreen>
#include<cmath>
/* TO DO:
 * omezit spektrum zezhora -  na khz fakt nic neni
 * potrebuju zvysit rozlisen ve spektralni ose - asi odstranit promerovani a udelat jeden spektrogram prez celou sirku okna

*/
class Spectrogram : public QObject
{
    Q_OBJECT
public:
    explicit Spectrogram(QObject *parent = nullptr);

    QPixmap * initSpectrogramPixmap();
    QPixmap *add_old_spectrogram(QPixmap * new_pixmap,int spectrogram_lenght); // arg because of size
    QGraphicsScene *initSpectrogramScene(QPixmap * pixmap);

    QPixmap * drawSpectrogramLine(QPixmap * spectrogram_pixmap,double * spectrum, int frame_lenght, int frame_count);
    void resetSpectrogram();

signals:
    void renderSpectrogram(QGraphicsScene * spectrogram_scene);

public slots:
    void updateSpectrogram(double * new_spectrum_line,int frame_lenght,int frame_count);

private:
    QObject * main_window;
    QGraphicsScene * spectrogram_scene;
    QPixmap * spectrogram_pixmap;
    void connectSignals();
    int spectrogram_width;
    int spectrogram_height;
    QColor getColor(double pixel_color_value);
    QColor getLogColor(double pixel_color_value);

    double * powerSpectrum(double * new_spectrum_line,int frame_lenght);
    double * powerLog(double * power_spectrum_line,int frame_lenght);
};

#endif // SPECTROGRAM_H
