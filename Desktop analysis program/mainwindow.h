#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDateTime>
#include <QThread>

#include "serialport.h"
#include "serialdialog.h"
#include "tcpport.h"
#include "tcpdialog.h"
#include "database.h"
#include "dataanalyzer.h"
#include "spectrogram.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void makePlot();

    void setupSpectrograms();
    void renderSpectrogram(); // funkce pro render kdyz se prijmou dalsi data




private slots:
    void on_actionCOM_triggered();
    void on_actionWiFi_triggered();
    void measure(); // funkce po kliknuti na merit

    void renderSpectrogram(QGraphicsScene * spectrogram_scene);





    void serialtest();
    void tcptest();

public slots:
    void setUiStatus(QString str);
    //void updateSpectrogram(double * spectrum_line,int frame_lenght);
signals:
    void sendCMD(char command);
    void setRecieveDataMode();
    void startMeasuring(char c);
    void initRealTimeFFT();




private:
    Ui::MainWindow *ui;
    SerialPort * serialport; // object containing qserialport
    SerialDialog * serialdialog;
    TcpPort * tcpport; // object containing tcpWIFIport
    TcpDialog * tcpdialog;
    Database * database;
    DataAnalyzer * dataanalyzer;
    Spectrogram * spectrogram_1;

    void connectUi();
    void init();


};
#endif // MAINWINDOW_H
