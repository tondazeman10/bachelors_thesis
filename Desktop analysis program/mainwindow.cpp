#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->serialport = new SerialPort(this);
    this->serialdialog = new SerialDialog(this, serialport);
    this->tcpport = new TcpPort(this);
    this->tcpdialog = new TcpDialog(this, tcpport);
    this->database = new Database(this, serialport);
    this->spectrogram_1 = new Spectrogram(this);
    this->dataanalyzer = new DataAnalyzer(this, database, tcpport,spectrogram_1);




    ui->IDEdit->setText(database->makeID());
    //qDebug() << QString::number(database->makeID());
    //qDebug() << database->makeID();
    connectUi();
    init();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::makePlot()
{
    /*
    // generate some data:
    QVector<double> x(101), y(101); // initialize with entries 0..100
    for (int i=0; i<101; ++i)
    {
      x[i] = i/50.0 - 1; // x goes from -1 to 1
      y[i] = x[i]*x[i]; // let's plot a quadratic function
    }
    // create graph and assign data to it:
    this->ui->customplot->addGraph();
    this->ui->customplot->graph(0)->setData(x, y);
    // give the axes some labels:
    this->ui->customplot->xAxis->setLabel("x");
    this->ui->customplot->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    this->ui->customplot->xAxis->setRange(-1, 1);
    this->ui->customplot->yAxis->setRange(0, 1);
    this->ui->customplot->replot();
    */
}

void MainWindow::setupSpectrograms()
{

}

void MainWindow::serialtest()
{
    qDebug() << "serial cmd test";
    serialport->sendCMD(3);
}

void MainWindow::tcptest()
{
    qDebug() << "tcp cmd test";
    //tcpport->testConnection();
    emit this->sendCMD(1);
}



void MainWindow::on_actionCOM_triggered()   //  button for COM settings
{
    serialdialog->show();
}

void MainWindow::on_actionWiFi_triggered()
{
    tcpdialog->show();
}

void MainWindow::measure()
{
/*
    serialport->setPortName(this->ui->PortBox->currentText());
    this->ui->baudBox->currentText().toInt();
    serialport->setBaudRate(this->ui->baudBox->currentText().toInt());
*/
     //qDebug() << this->ui->lenghtBox->currentText().toInt();
     //qDebug() << this->ui->heightEdit->text().toInt();
     //serialport->measure(this->ui->lenghtBox->currentText().toInt());

     // sekvence funkci : 1 - setup files 2 - start measuring 3 - wait for end 4 - convert data, save and do analysis
    database->makeTXT(database->makeID());
    database->makeCSV(database->makeID());
    emit sendCMD(3); // cmd measure
    char c = static_cast<char>(this->ui->lenghtBox->currentText().toInt());
    emit startMeasuring(c); // send duration in secs
    emit setRecieveDataMode(); // exits automatically
    emit initRealTimeFFT();

}

void MainWindow::renderSpectrogram(QGraphicsScene * spectrogram_scene)
{
    qDebug() << "rendering spk";
    this->ui->spectrogram1_View->setAlignment(Qt::AlignLeft);
    this->ui->spectrogram1_View->setScene(spectrogram_scene);
    //this->ui->spectrogram1_View->setHorizontalScrollBar(s)
}


void MainWindow::connectUi()
{
   connect(this->ui->measureButton,SIGNAL(clicked()),this,SLOT(measure()));

   // debug komunikace
   connect(this->ui->stest,SIGNAL(clicked()),this,SLOT(serialtest()));
   connect(this->ui->ttest,SIGNAL(clicked()),this,SLOT(tcptest()));

   //connect(this,SIGNAL(sendCMD()),serialport,SLOT(sendCMD()));
}

void MainWindow::init()
{
    //connect(this,SIGNAL(sendCMD(char command))),tcpport,SLOT(sndCMD(char command)));
    connect(this,SIGNAL(sendCMD(char)),tcpport,SLOT(sendCMD(char))); //connected()
    connect(this,SIGNAL(setRecieveDataMode()),tcpport,SLOT(setRecieveDataMode()));
    connect(this->tcpport,SIGNAL(outputDATA(QByteArray *)),this->database,SLOT(inputDATA(QByteArray *)));
    connect(this->tcpport,SIGNAL(outputDATAframe(QByteArray *)),this->database,SLOT(inputDATAframe(QByteArray *)));
    connect(this->database,SIGNAL(deallock()),this->tcpport,SLOT(deallocArrays()));

    connect(this,SIGNAL(startMeasuring(char)),tcpport,SLOT(startMeasuring(char)));

    // connecting dataAnalyser

    //connect(this,SIGNAL(initRealTimeFFT()),dataanalyzer,SLOT(initRealTimeFFT()));

}

void MainWindow::setUiStatus(QString str)
{
    this->ui->connection_label->setText(str);
}


