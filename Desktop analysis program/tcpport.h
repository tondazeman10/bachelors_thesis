#ifndef TCPPORT_H
#define TCPPORT_H

#include <QObject>

#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
#include <QString>
#include <QThread>

/*  PRINCIP FUNKCE
 * Tcp socket connection bude otevreny porad a bude se posilat idle ack aby se komunikace udrzela spojena
 * napriklad kazdou vterinu:
 * APP - sends idle char and waits for ACK
 * ESP - sends ACK
 * APP - recieves - after 1 second do again
 * APP - doesnt recieve - attepmt reconnecting - if fails pop up ERROR message
 *
*/
/* TO OD:
 * alokace velkych poli pro namerena data, staticky to rozhodne nebude fungovat pro dlouhy casy ... nasledne nejake mezipredavani, ale to asi ne v ramci BK - DONE
 * uprimne multichannel taky asi nebude v ramci BK - pffff
 * BUG : prvni hodnota v kazdem mereni je divna protoze ji chybi MSB cast cisla, v kazdym poli na zacatku je 1 nevim proc - u vahovani to asi stejne nebude mit vliv
 *          -- kdyz se neposila ack tak to nedela, bud bez ack nebo to nejak delayem odstranit
 * dodelat lepsi nastavovani delky analyzovaneho framu, t5eba z ui
*/
class TcpPort : public QObject
{
    Q_OBJECT
private:
    QObject * mainwindow;
    QTcpServer * server;// use more  sockets for multichannel measuring
    QTcpSocket * socket1;
    QTcpSocket * socket2;
    QTcpSocket * socket3;
    QTcpSocket * socket4;


    bool connected;
    bool recieve_mode;
public:
    QByteArray * recieved_data1; // dodelat alokaci , velikost je dana vstupnimy parametry formulare
    QByteArray * recieved_data2;
    QByteArray * recieved_data3;
    QByteArray * recieved_data4;
    explicit TcpPort(QObject *parent = nullptr);

    // CMD's for ESP
    void cmdMeasure(char duration);
    void testConnection();


    char readCMD();


    void exitRecieveDataMode();



signals:
    void outputDATA(QByteArray * output_array);
    void outputDATAframe(QByteArray * output_array);
    void endOfDATA();
    void setUiStatus(QString status);


public slots:
    void openPort();
    void closePort();
    void newConnection();

    void setRecieveDataMode();
    void readDATA();
    bool sendCMD(char c);
    void deallocArrays();
    void reallocArrays();
    void startMeasuring(char c);
    //void openPort();

    //void sendCMD(char c);
    //int readCMD();
    //void read();
};

#endif // TCPPORT_H
