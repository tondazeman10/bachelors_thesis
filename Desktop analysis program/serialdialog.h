#ifndef SERIALDIALOG_H
#define SERIALDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QSettings>
#include <QVariant>
#include <QSerialPortInfo>
#include <QDebug>

#include "serialport.h"

namespace Ui {
class SerialDialog;
}

class SerialDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SerialDialog(QWidget *parent = nullptr, SerialPort * serialport = nullptr);
    ~SerialDialog();

    void fillPorts();

private slots:

    void on_connectButton_clicked();
    void hideSerialDialog();

signals:

private:
    Ui::SerialDialog *ui;
    SerialPort * serialport;
};

#endif // SERIALDIALOG_H
