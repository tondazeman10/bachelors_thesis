#include "database.h"

Database::Database(QObject *parent, SerialPort * serialport,  TcpPort * tcpport) : QObject(parent)
{
    this->main_window = parent;
    this->serialport = serialport;
    this->tcpport = tcpport;
    subjectCount = 0;

    connect(serialport,SIGNAL(incrementID()),this,SLOT(incrementSubjectCount()));
    //connect(tcpport,SIGNAL(endOfData()),this,SLOT(endOfInputDATA()));


    //connect(tcpport,SIGNAL(outputDATA()),this,SLOT(inputDATA())); // after all session data is accepted

    setupDir();

    // test for data reconstruction
    /*
    QByteArray * arr = new QByteArray;
    char c = 1;
    char x = 1;
    char z = 5;
    arr->append(c);
    arr->append(x);

    arr->append(z);
    arr->append(z);
    arr->append(z);

    qDebug() << "arr:";
    qDebug() << arr;
    recunstructData(arr);

     0100111
    0010011100010000 10 000
    0001000000100111  4135
    */


}

void Database::setupDir()
{
     dir = new QDir;
     if(dir->exists("database")){
        return;
     }
     if(!dir->mkdir("database")){
         qDebug() << "ERROR: could not make data directory";
     }
}

void Database::makeTXT(QString file_name)
{
    file = new QFile("database/" + file_name + ".txt");
    //qDebug() << "making txt";
    if(file->exists("database/" + file_name + ".txt")){
       qDebug() << "ERROR: measurement txt with this ID already exists";
       return;
    }
    file->open(QFile::WriteOnly);
    file->flush();
    file->close();
}

void Database::makeCSV(QString file_name)
{
    file = new QFile("database/" + file_name + ".csv");
    //qDebug() << "making txt";
    if(file->exists("database/" + file_name + ".csv")){
       qDebug() << "ERROR: measurement csv with this ID already exists";
       return;
    }
    file->open(QFile::WriteOnly);
    file->flush();
    file->close();
}

QString Database::makeID()  // format: ddmmyyyyhhmmiii - subject number
{
    QTime time;
    QDate date;
    date = date.currentDate();
    time = time.currentTime();

    QString subject = "0";
    QString s_day;
    QString s_month;
    QString s_year;
    QString s_hour;
    QString s_minute;

    if(date.day() < 10){    // making single digit values double digit for stable format
        s_day = "0" + QString::number(date.day());
    }
    else {
        s_day = QString::number(date.day());
    }

    if(date.month() < 10){
       s_month  = "0" + QString::number(date.month());
    }
    else {
        s_month  = QString::number(date.month());
    }

    s_year = QString::number(date.year());

    if(time.hour() < 10){
       s_hour = "0" + QString::number(time.hour());
    }
    else {
        s_hour = QString::number(time.hour());
    }

    if(time.minute() < 10){
       s_minute = "0" + QString::number(time.minute());
    }
    else {
        s_minute = QString::number(time.minute());
    }


    if(this->subjectCount < 10){    // make subject number double digit for < 10
        subject = "00" + QString::number(this->subjectCount);
    }
    else if(this->subjectCount < 100){
        subject = "0" + QString::number(this->subjectCount);
    }
    else {
        subject = QString::number(this->subjectCount);
    }

    QString ID = s_day + s_month + s_year + s_hour + s_minute + subject;

    //qDebug() << subject;
    //qDebug() << ID;
    //qDebug() << ID.toInt();
    //incrementSubjectCount();
    this->currentID = ID;
    incrementSubjectCount();
    return ID;
}

void Database::saveDataToFile(QString file_name, uint16_t * dataArray, int samples)
{
    file = new QFile(); //"database/" + file_name + ".csv"
    file->setFileName(file_name);
    //file->open(QFile::WriteOnly);
    file->open(QFile::Append);
    QTextStream in(file);
    for(int i=0;i<samples;i++){

        in << QString::number(dataArray[i]) + ',';
    }
    file->flush();
    file->close();
    emit deallock();
}
void Database::incrementSubjectCount()
{
    this->subjectCount = this->subjectCount + 1;
}

void Database::inputDATA(QByteArray * input_data)   // tahle funkce se pouzije pokud tcpport najde konec dat, vetsinou uz jsou to jen ty 3x5
{
    qDebug() << "Database recieved data";
    qDebug() << input_data->length();
    uint16_t * placeholder;
    placeholder = new uint16_t[(input_data->length()-4)/2]; // remove EOD and 2 bytes is one sample
    unsigned char x,y;
    for(int i=0;i < input_data->length()-3;i=i+2){ // 0,2,4  -nezacinat na indexu nula je tam divnej znak kteryho se neumim zbavit ...
        if(input_data->at(i) < 0){  // konverze ze signed to unsigned
            x = static_cast<unsigned char>(input_data->at(i)+256);
        }
        else {
            x = static_cast<unsigned char>(input_data->at(i));
        }
        if(input_data->at(i+1) < 0){  // konverze ze signed to unsigned
            y = static_cast<unsigned char>(input_data->at(i+1)+256);
        }
        else {
            y = static_cast<unsigned char>(input_data->at(i+1));
        }
        // ted mam spravny hezky cisla
        placeholder[i/2] = static_cast<uint16_t>(x << 8 | y); // bitovy poskladani zpatky
        //qDebug() << placeholder[i/2];
    }
    qDebug() << "Database reconstructed data";
    saveDataToFile("database/" + this->currentID + ".csv", placeholder, (input_data->length()-4)/2);
}

void Database::inputDATAframe(QByteArray *input_data) // // tahle funkce se pouzije pokud tcpport nenajde konec dat
{
    qDebug() << "Database recieved data frame";
    qDebug() << input_data->length();
    int frame_lenght = (input_data->length())/2;
    uint16_t * placeholder;
    placeholder = new uint16_t[(input_data->length())/2]; //  2 bytes is one sample
    unsigned char x,y;
    for(int i=0;i < input_data->length();i=i+2){ // 0,2,4  -nezacinat na indexu nula je tam divnej znak kteryho se neumim zbavit ...
        if(input_data->at(i) < 0){  // konverze ze signed to unsigned
            x = static_cast<unsigned char>(input_data->at(i)+256);
        }
        else {
            x = static_cast<unsigned char>(input_data->at(i));
        }
        if(input_data->at(i+1) < 0){  // konverze ze signed to unsigned
            y = static_cast<unsigned char>(input_data->at(i+1)+256);
        }
        else {
            y = static_cast<unsigned char>(input_data->at(i+1));
        }
        // ted mam spravny hezky cisla
        placeholder[i/2] = static_cast<uint16_t>(y << 8 | x); // bitovy poskladani zpatky


      //void calculateFrameFFT(uint16_t * dataIn,int frame_lenght);
    }
    //for(int i=0;i<input_data->length()/2;i++){
    //    qDebug() << placeholder[i];
    //}
    emit calculateFrameFFT(placeholder, frame_lenght);
    qDebug() << "Database reconstructed data frame";
    saveDataToFile("database/" + this->currentID + ".csv", placeholder, (input_data->length())/2);
}

void Database::endOfInputDATA() // zda se ze neni potreba
{
    emit deInitRealTimeFFT();
}




