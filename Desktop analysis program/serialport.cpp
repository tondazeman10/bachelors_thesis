#include "serialport.h"
SerialPort::SerialPort(QObject *parent) :  QSerialPort(parent)
{
    this->mainwindow = parent;
    this->qserialport = new QSerialPort;
    this->connected = false;
    //connect(this,SIGNAL(displayTextCOM()),mainwindow,SLOT(displayTextCOM())); // reads serial at all times, not really usable
}

bool SerialPort::getConnectionState()
{
    return this->connected;
}

void SerialPort::changeConnectionState(bool state)
{
    this->connected = state;
}

void SerialPort::setRecieveDataMode()
{
    connect(this->qserialport,SIGNAL(readyRead()),this,SLOT(readDATA())); // read socket at all times
}

void SerialPort::exitRecieveDataMode()
{
    // disconnect continuous reading
    // transfer data from buffer
    // increment id
    qDebug() << "EOD - returning to cmd state";
    disconnect(this->qserialport,SIGNAL(readyRead()),this,SLOT(read())); // change state to cmd reading
}

void SerialPort::sendCMD(char c)
{
    QByteArray cmd;
    char x;
    if(this->connected == false){
        qDebug() << "ERROR: Serial port no open, could not send cmd.";
        return;
    }
    cmd[0] = c;
    this->qserialport->write(cmd);
    this->qserialport->flush();
    qDebug() << "CMD sent";
    this->qserialport->waitForBytesWritten(1000);
    x = readCMD();
    if(!x){
        qDebug() << "ERROR:ACK not recieved!";
        qDebug() << x;
    }
    else if(x=='1'){
        qDebug() << "ACK recieved!";
        qDebug() << "CMD sequence succesfull";
    }
    else{
        qDebug() << "ERROR: incorrect ACK recieved!";
        qDebug() << x;
    }
}

char SerialPort::readCMD()
{
    char c;
    if(this->qserialport->bytesAvailable() > 0){
        this->qserialport->getChar(&c);
        return  c;
    }
    else{
        return 0;
    }
}

void SerialPort::readDATA() // for state
{

    // mozna pouzit wait for bytes available - potom kouknout na konec retezce a vyhodit error kdyz to nejsou 3x 5


    // scand for end of data sequence - 555
    char * buf;
    buf = new char[qserialport->bytesAvailable()];
    qserialport->peek(buf, sizeof(buf));
    //char buf[qserialport->bytesAvailable()];
    //qserialport->peek(buf, sizeof(buf)
    //qserialport->bytesAvailable();
    for(int i=0;i<qserialport->bytesAvailable();i++){   // end of data sequence check
        if(buf[i] == 5 && buf[i+1] == 5 && buf[i+2] == 5){  // problem pokud prijde 5 v dalsim segmentu
            exitRecieveDataMode();
            return;
        }
        else if(buf[i] == 5 && buf[i+1] == 5){
            char * c;
            for(int j=0;j<1000;j++){} // delay - mohl by  vadit dlouhy delay pro spatne interpretaci konce
            c = new char[qserialport->bytesAvailable()]; // look forward for other 5s
            qserialport->peek(c, sizeof(c));

            if(c[0] == 5){
                exitRecieveDataMode();
                return;
            }
        }
        else if(buf[i] == 5){
            char * c;
            for(int j=0;j<2000;j++){} // delay
            c = new char[qserialport->bytesAvailable()]; // look forward for other 5s
            qserialport->peek(c, sizeof(c));

            if(c[0] == 5 && c[1] == 5){
                exitRecieveDataMode();
                return;
            }
        }
    }
    recieved_data+= qserialport->readAll(); // append QBytearray with recived data
    delete buf;
}
void SerialPort::setPortName(QString name)
{
    this->port_name = name;
    qDebug() << "setting portname";
}

void SerialPort::setBaudRate(int baudrate)
{
   this->baud_rate = baudrate;
   qDebug() << "setting baudrate";
   qDebug() << baudrate;
}



void SerialPort::cmdMeasure(char duration)
{
    qDebug() << "measure sequence";
    sendCMD(3); // start measure sequence
    if(this->readCMD() != 1){ // ack check
        qDebug() << "no ACK recieved";
        return;
    }
     sendCMD(duration);
     //recieveData();
}
void SerialPort::openPort()
{
    qDebug() << "creating serial port";
    qDebug() << this->port_name;
    qDebug() << this->baud_rate;
    // changeble parameters
    if(this->port_name != ""){
        //this->setPortName("COM5");
        this->qserialport->setPortName(this->port_name);
    }
    else {
        qDebug() << "port not selected!";
    }
    if(this->baud_rate != 0){
         this->qserialport->setBaudRate(this->baud_rate);
        // QSerialPort::Baud115200
        //this->setBaudRate(QSerialPort::Baud115200);
    }
    else {
        qDebug() << "baudrate not selected!";
    }
    // fixed parameters
    this->qserialport->setDataBits(QSerialPort::Data8);
    this->qserialport->setParity(QSerialPort::NoParity);
    this->qserialport->setStopBits(QSerialPort::OneStop);
    this->qserialport->setFlowControl(QSerialPort::NoFlowControl);
    //const auto infos = QSerialPortInfo::availablePorts();
    //qDebug() << QSerialPortInfo::availablePorts();
    qDebug() << "win condition check";

    if (this->qserialport->open(QIODevice::ReadWrite)) {
        qDebug() << "Serial opened!";
        changeConnectionState(true);
        emit hideSerialDialog();
        emit displayTextCOM();
    } else {
        qDebug() << "Failed to open Serial";
    }
}
