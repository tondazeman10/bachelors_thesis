#include "serialdialog.h"
#include "ui_serialdialog.h"

SerialDialog::SerialDialog(QWidget *parent, SerialPort * serialport) : QDialog(parent), ui(new Ui::SerialDialog)
{
    ui->setupUi(this);
    this->fillPorts();
    this->serialport = serialport;
    connect(this->ui->connectButton,SIGNAL(clicked()),serialport,SLOT(openPort())); // i thing signal should be slot
    connect(serialport,SIGNAL(hideSerialDialog()),this,SLOT(hideSerialDialog()));
    //connect(this->ui->pushButton_2,SIGNAL(clicked()),serialport,SLOT(sendCMD(char c))); // debug
}

SerialDialog::~SerialDialog()
{
    delete ui;
}

void SerialDialog::fillPorts()  // add available com ports to dialog
{
    SerialDialog::ui->PortBox->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        list << info.portName();

        SerialDialog::ui->PortBox->addItem(list.first(), list);
    }
}

void SerialDialog::on_connectButton_clicked()  // button connect clicked
{
    //
    //this->hide();
    serialport->setPortName(this->ui->PortBox->currentText());
    this->ui->baudBox->currentText().toInt();
    serialport->setBaudRate(this->ui->baudBox->currentText().toInt());
}

void SerialDialog::hideSerialDialog()
{
    this->hide();
}
