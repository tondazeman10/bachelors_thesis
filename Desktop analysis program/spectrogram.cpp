#include "spectrogram.h"

Spectrogram::Spectrogram(QObject *parent) : QObject(parent)
{

    QScreen *screen = QApplication::screens().at(0);
    this->spectrogram_width = screen->size().width()/3;
    this->main_window = parent;
    this->spectrogram_pixmap = initSpectrogramPixmap();
    this->spectrogram_scene = initSpectrogramScene(spectrogram_pixmap);
    connectSignals();
    emit renderSpectrogram(spectrogram_scene); // render blank spectrogram
    //this->spectrogram_pixmap=drawSpectrogramLine(spectrogram_pixmap, double *spectrum, int frame_lenght)
}
void Spectrogram::connectSignals()
{
    connect(this,SIGNAL(renderSpectrogram(QGraphicsScene*)),main_window,SLOT(renderSpectrogram(QGraphicsScene*)));
}

QColor Spectrogram::getColor(double amplitude_value) // vraci barvu pro ampl spectrogram
{
    // RGB zobrazeni, postup gradientu:

    //  0,0,0 -> 0,0,255 ---- 255
    //  0,0,255 -> 0,255,255 ---- 510
    //  0,255,255 -> 0,255,0 ---- 765
    //  0,255,0 -> 255,255,0 ---- 1020
    //  255,255,0 -> 255,0,0 ---- 1275

    // rozsah = 5*256=1280 barevnych hodnot
    // vstupni data 65536

    // 65536 / 1280 = 51.2
    // konstanta na namapovani na amplitudove spektrum
    // prirazeni barevnostniho cisla bodu ve spektru

    int color_value = static_cast<int>(amplitude_value/51.2+0.5); // +0.5 kvuli static cast vzdy zaokrouhluje dolu


    if(color_value <= 255){
        return QColor(0,0,color_value);
    }
    else if(color_value > 255 && color_value <= 510){
        return QColor(0,color_value-255,255);
    }
    else if(color_value > 510 && color_value <= 765){
        return QColor(0,255,765-color_value);
    }
    else if(color_value > 765 && color_value <= 1020){
        return QColor(color_value-765,255,0);
    }
    else if(color_value > 1020 && color_value <= 1275){ // myslim ze maximum je na 1280
        return QColor(255,1275-color_value,0);
    }
    else{
        qDebug() << "couldnt find the right color";
        return Qt::black;

    }

}

QColor Spectrogram::getLogColor(double log_color_value) // vraci barvu pro log spectrogram
{
    // NAMAPOVANI LOG HODNOTY NA 0-1275 SCALE
    // COLOR = LOG * 19.25;

    int color_value = static_cast<int>(log_color_value * 19.25+0.5); // +0.5 kvuli static cast vzdy zaokrouhluje dolu

    if(color_value <= 0){ // opratreni pro zaporne logaritmy, uvidime jestli se to pak taky nenamapuje ale asi je to zbyte4n7
       return Qt::black;
    }
    else if(color_value <= 255){
        return QColor(0,0,color_value);
    }
    else if(color_value > 255 && color_value <= 510){
        return QColor(0,color_value-255,255);
    }
    else if(color_value > 510 && color_value <= 765){
        return QColor(0,255,765-color_value);
    }
    else if(color_value > 765 && color_value <= 1020){
        return QColor(color_value-765,255,0);
    }
    else if(color_value > 1020 && color_value <= 1275){ // myslim ze maximum je na 1280
        return QColor(255,1275-color_value,0);
    }
    else{
        qDebug() << "couldnt find the right color";
        return Qt::black;
    }
}

double *Spectrogram::powerSpectrum(double *new_spectrum_line, int frame_lenght)
{
    double * powr_spectrum_line = new double[frame_lenght/2];
    for(int i=0;i<frame_lenght/2;i++){
        powr_spectrum_line[i] = (new_spectrum_line[i] * new_spectrum_line[i])/(frame_lenght/2);
        //qDebug() << powr_spectrum_line[i];
    }
    return powr_spectrum_line;
}

double *Spectrogram::powerLog(double *power_spectrum_line, int frame_lenght)
{
    double * log_spectrum_line = new double[frame_lenght/2];
    for(int i=0;i<frame_lenght/2;i++){
        log_spectrum_line[i] = 10*log10(power_spectrum_line[i]);
        qDebug() << log_spectrum_line[i];
    }
    return log_spectrum_line;
}
QPixmap * Spectrogram::initSpectrogramPixmap()
{
    QPixmap * pixmap;
    QScreen *screen = QApplication::screens().at(0);
    int frame_lenght = 2048; // z toho vyplyva pocet bodu spektra a vyska obrazku
    spectrogram_height = frame_lenght / 2 ; // 1024/4 -- 256 bodu
    spectrogram_width = 1000;
    //qDebug() << screen->size().width();
    //qDebug() << screen->size().height();

    // pro ntb monitory se to upravi napr /2 cojavim
    //int width = screen->size().width()/3;
    // set dimensions for calculating, background
    pixmap = new QPixmap(spectrogram_width,spectrogram_height);
    pixmap->fill(Qt::black);
    return pixmap;
}


QGraphicsScene *Spectrogram::initSpectrogramScene(QPixmap * pixmap)
{
    QGraphicsScene * scene = new QGraphicsScene(this);
    return scene;
}

void Spectrogram::updateSpectrogram(double *new_spectrum_line, int frame_lenght,int frame_count)
{

    // 65536 / 1280 = 51.2
    // konstanta na namapovani na amplitudove spektrum
    // prirazeni barevnostniho cisla bodu ve spektru

    QPen paintPen(Qt::black);
    paintPen.setWidth(1); // podle velikosti displaye - dodelat

    double * power_spectrum = new double[frame_lenght/2]; // spocitani PSD
    power_spectrum = powerSpectrum(new_spectrum_line, frame_lenght);
    power_spectrum = powerLog(power_spectrum,frame_lenght); // zlogaritmovani

    //spectrogram_scene->setBackgroundBrush(Qt::black);
    //amplitudove zobrazeni

    /*
    for(int i=0;i<spectrogram_height;i++){  // paint one gradient column
        //QColor * color = new QColor(i,0,0); // logika prirazeni barvy
        QPen paintPen(getColor(new_spectrum_line[i]));
        spectrogram_scene->addRect(frame_count,spectrogram_height-i,1,1,paintPen);// x,y,width, height, pen
    }
    */
    // log zobrazeni
    for(int i=0;i<spectrogram_height;i++){  // paint one gradient column
        //QColor * color = new QColor(i,0,0); // logika prirazeni barvy
        QPen paintPen(getLogColor(power_spectrum[i]));
        spectrogram_scene->addRect(frame_count,spectrogram_height-i,1,1,paintPen);// x,y,width, height, pen
    }
}


