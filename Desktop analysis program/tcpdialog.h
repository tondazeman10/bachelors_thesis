#ifndef TCPDIALOG_H
#define TCPDIALOG_H

#include <QDialog>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>

#include "tcpport.h"

namespace Ui {
class TcpDialog;
}

class TcpDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TcpDialog(QWidget *parent = nullptr, TcpPort * tcpport = nullptr);
    ~TcpDialog();

private:
    Ui::TcpDialog *ui;
    TcpPort * tcpport;
public slots:
    void setUiStatus(QString str);
};

#endif // TCPDIALOG_H
