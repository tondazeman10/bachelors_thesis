#include "dataanalyzer.h"


#define REAL 0
#define IMAG 1
#define PI 3.14159265358979323846
DataAnalyzer::DataAnalyzer(QObject * parent, QObject * database, QObject * tcpport,QObject * spectrogram)
{
    this->main_window = parent;
    this->database = database;
    this->tcp_port = tcpport;
    this->spectrogram_1 = spectrogram;

    connectSingnals();


}

int DataAnalyzer::getFrameCount()
{
    return this->frame_counter;
}
void DataAnalyzer::connectSingnals()
{
    qDebug() << "privadec";
    connect(this->main_window,SIGNAL(initRealTimeFFT()),this,SLOT(initRealTimeFFT()));
    connect(this->database,SIGNAL(deInitRealTimeFFT()),this,SLOT(deInitRealTimeFFT()));
    connect(tcp_port,SIGNAL(endOfDATA()),this,SLOT(deInitRealTimeFFT()));
    connect(this,SIGNAL(updateSpectrogram(double*,int,int)),spectrogram_1,SLOT(updateSpectrogram(double*,int,int)));

}
void DataAnalyzer::initRealTimeFFT()
{
    qDebug() << "Realtime FFT running";
    // connectne to realTimeFFT funkci s prijetim jednoho framu aby se to hned delalo
    // trackovat pocet framu aby se vedelo jak poskladavat spectrogram
    connect(this->database,SIGNAL(calculateFrameFFT(uint16_t *,int)),this,SLOT(realTimeFrameFFT(uint16_t *,int)));
    this->frame_counter = 0;
}

void DataAnalyzer::deInitRealTimeFFT()
{
    qDebug() << "Analyser deinited";
    disconnect(this->database,SIGNAL(calculateFrameFFT(uint16_t *,int)),this,SLOT(realTimeFrameFFT(uint16_t *,int)));
    this->frame_counter = 0;
}
double * DataAnalyzer::fft(uint16_t *in/*,double * out,*/, int frame_lenght)
{
    // ja budu mit na vstupu 4096 / 2048! vzorku - naoknovat, narvat do cplx pole, udelat abs, vyndat zas normalni cisla
    double * out = new double[frame_lenght];
    fftw_complex *cplx_in, * cplx_out;
    fftw_plan plan;
    cplx_in = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * static_cast<unsigned int>(frame_lenght)));
    cplx_out = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * static_cast<unsigned int>(frame_lenght)));

    // naplneni vstupniho pole daty
    for(int i = 0; i < frame_lenght; ++i) {
        cplx_in[i][REAL] = in[i];		// data pouze do realne casti
        cplx_in[i][IMAG] = 0;
    }

    // create a DFT plan
    plan = fftw_plan_dft_1d(frame_lenght, cplx_in, cplx_out, FFTW_FORWARD, FFTW_ESTIMATE);
    // execute the plan
    fftw_execute(plan);
    // do some cleaning
    fftw_destroy_plan(plan);
    fftw_cleanup();

    //double x = cplx_out[0][REAL];
    for(int i = 0; i < frame_lenght; ++i) {
        //out[i] = abs(static_cast<double>(cplx_out[i][REAL]) + abs(static_cast<double>(cplx_out[i][IMAG])); // nejde tam narvat doubl, pokud speed - dat do floatu a udelat pomoci abs
        out[i] =(sqrt(cplx_out[i][REAL]*cplx_out[i][REAL] + cplx_out[i][IMAG]*cplx_out[i][IMAG]))/frame_lenght; // prepocitani abs hodnoty z trojuhelnikum, pro rychlost nedelat v doublech
        out[i] = out[i]; // - korekce windowingu
        //qDebug() << out[i];
        // dc slozka je spravne velka ve spektru -> nejspis je spravne
        //qDebug() << i;
    }

    // na dealokaci poli fftw doublu fftw_free()
    fftw_free(cplx_in);
    fftw_free(cplx_out);
    return out;
}
uint16_t * DataAnalyzer::hann_window(uint16_t * dataIn, int frame_lenght) // korekce amplitudy = 2 korekce energie = 1.63
{
    uint16_t * dataOut = new uint16_t[frame_lenght];
    for (int i = 0; i < 2048; i++) {
        double multiplier = 0.5 * (1 - cos(2*PI*i/2047));
        dataOut[i] = static_cast<uint16_t>(multiplier * dataIn[i]);
    }
    return dataOut;
}
double *DataAnalyzer::oneSideSpectrum(double *two_sided_spectrum, int frame_lenght)
{
    double * one_sided_spectrum = new double[frame_lenght/2];
    one_sided_spectrum[0] = two_sided_spectrum[0];
    for(int i=1;i<frame_lenght/2;i++){ // delka by mela byt l/2+1 ale vrchni vzorek zahodim protoze chci even cislo
        one_sided_spectrum[i] = 2*two_sided_spectrum[i];
    }
    return one_sided_spectrum;
}

uint16_t * DataAnalyzer::fftExampleData()
{   // parametry testovaci sinusovky

    int A=32000;
    double f=4000;
    double fs=16129;
    // 16 129 Hz je frekvence LSA, chci pouzivat 2048 framy
    double f_mapped_to_fs = f / fs; // je to premapovany protoze moje vterina ma 16129 bodu
    //qDebug() << f_mapped_to_fs;
    //qDebug() << t;
    //double * data = new double[2048];
    uint16_t * idata = new uint16_t[2048];
    for(int i=0;i<2048;i++){
        idata[i] =static_cast<uint16_t>(32000 + A*sin(2*PI*i*f_mapped_to_fs)); // simuluje vstup z adc
    }
    for(int i=0;i<2048;i++){
        //qDebug() << idata[i];
    }

    return  idata;
}
void DataAnalyzer::realTimeFrameFFT(uint16_t * dataIn, int frame_lenght)
{
    qDebug() << "fft of one frame done";

    //dataIn = new uint16_t[frame_lenght];
    double * dataOut = new double[frame_lenght];
    double * one_side_out = new double[frame_lenght/2];

    //in = fftExampleData();
    dataIn = hann_window(dataIn,frame_lenght);
    dataOut = fft(dataIn, frame_lenght); // napojeni sinusovky do spectrogramu
    one_side_out = oneSideSpectrum(dataOut, frame_lenght);
    /*
    for(int i=0;i<frame_lenght/2;i++){
        qDebug() << one_side_out[i];
    }
    */
    emit updateSpectrogram(one_side_out,frame_lenght,frame_counter);
    this->frame_counter++;
}

void DataAnalyzer::memoryDataFFT()
{

}
