#include "tcpport.h"

TcpPort::TcpPort(QObject *parent) : QObject(parent)
{
    server= new QTcpServer(this);
    mainwindow = parent;
    this->connected = false;
    this->recieve_mode = false;

   // alokace pro logiku vybrani spravneho kanalu
   socket1 = new QTcpSocket;
   socket2 = new QTcpSocket;
   socket3 = new QTcpSocket;
   socket4 = new QTcpSocket;

   recieved_data1 = new QByteArray;
   recieved_data2 = new QByteArray;
   recieved_data3 = new QByteArray;
   recieved_data4 = new QByteArray;

    connect(server,SIGNAL(newConnection()),this,SLOT(newConnection()));
    connect(this,SIGNAL(setUiStatus(QString)),mainwindow,SLOT(setUiStatus(QString)));

    //connect(this->socket1,SIGNAL(disconnected()),mainwindow,SLOT(setUiStatusConnected));

    openPort();
}
void TcpPort::testConnection()
{
    qDebug() << "testing connection measure cmd";
    if(sendCMD(3)){
       setRecieveDataMode();
    }
    for(int i=0;i<10000;i++){}
    qDebug() << recieved_data1;
    //setRecieveDataMode();
    /*
    QByteArray str("FF");
    bool ok;
    int hex = str.toInt(&ok, 16);     // hex == 255, ok == true
    int dec = str.toInt(&ok, 10);     // dec == 0, ok == false
    */
    /*
    int arr[1000];
    //arr = new int[1000];
    for(int i =0;i<1000;i++){
      char c = recieved_data1[i];
      if(c != 0){
          arr[i] = c;
      }
      //int val = int(c);
      qDebug() << arr ; // recieved_data1[i]
    }
    */

}

bool TcpPort::sendCMD(char c)
{
    QByteArray cmd;
    char x;
    if(this->connected == false){
        qDebug() << "ERROR: Tcp socket not open, could not send cmd.";
        return false;
    }
    cmd[0] = c;
    this->socket1->write(cmd);
    this->socket1->flush();
    qDebug() << "CMD sent";
    this->socket1->waitForBytesWritten(1000);
    //qDebug() << this->socket1->bytesAvailable();
    QThread::msleep(100);
    x = readCMD();
    if(!x){
        qDebug() << "ERROR:ACK not recieved!";
        return false;
    }
    else if(x==1){
        qDebug() << "ACK recieved!";
        qDebug() << "CMD sequence succesfull";
        return true;
    }
    else{
        qDebug() << "ERROR: incorrect ACK recieved!";
        qDebug() << x;
        qDebug() << static_cast<int>(x);
        return false;
    }
}

void TcpPort::deallocArrays()
{
    delete recieved_data1;
    delete recieved_data2;
    delete recieved_data3;
    delete recieved_data4;
}

void TcpPort::reallocArrays()
{
    recieved_data1 = new QByteArray;
    recieved_data2 = new QByteArray;
    recieved_data3 = new QByteArray;
    recieved_data4 = new QByteArray;
}

void TcpPort::startMeasuring(char c)
{
    this->socket1->putChar(c);
    this->socket1->flush();
}

char TcpPort::readCMD()
{
    char c;
    qDebug() << socket1->bytesAvailable();
    if(this->socket1->bytesAvailable() == 1){
        this->socket1->getChar(&c);
        return  c;

    }
    else if(this->socket1->bytesAvailable() > 0){
        this->socket1->getChar(&c);
        qDebug() << "ERROR : multiple cmd input";
        return  c;
    }
    else{
        qDebug() << "ERROR : expected cmd input";
        return 0;
    }
}

void TcpPort::openPort()
{
    if(!server->listen(QHostAddress::AnyIPv4,1234)){    // tells server to listen for connections
        qDebug() << "ERROR : failed opening tcp server";

    }
    else{
        qDebug() << "Listening for tcp connections";
    }
}

void TcpPort::closePort()
{
    socket1->disconnect();
    socket2->disconnect();
    socket3->disconnect();
    socket4->disconnect();
    server->close();
    emit setUiStatus("Odpojeno");
}



void TcpPort::newConnection() // server found pending connection
{
    if(socket1->state() == QAbstractSocket::UnconnectedState){
        socket1 = server->nextPendingConnection();
        this->connected = true;
        QByteArray cmd;
        cmd[0] = 1;
        this->socket1->write(cmd);
        this->socket1->flush();
        this->socket1->readAll();
        emit setUiStatus("Připojeno přes WiFi");
    }
    else if(socket2->state() == QAbstractSocket::UnconnectedState){
        socket2 = server->nextPendingConnection();
        this->connected = true;
        QByteArray cmd;
        cmd[0] = 1;
        this->socket1->write(cmd);
        this->socket2->flush();
        emit setUiStatus("Připojeno přes WiFi");
    }
    else if(socket3->state() == QAbstractSocket::UnconnectedState){
        socket3 = server->nextPendingConnection();
        this->connected = true;
        QByteArray cmd;
        cmd[0] = 1;
        this->socket1->write(cmd);
        this->socket3->flush();
        emit setUiStatus("Připojeno přes WiFi");
    }
    else if(socket4->state() == QAbstractSocket::UnconnectedState){
        socket4 = server->nextPendingConnection();
        this->connected = true;
        QByteArray cmd;
        cmd[0] = 1;
        this->socket1->write(cmd);
        this->socket4->flush();
        emit setUiStatus("Připojeno přes WiFi");
    }
    else{
        qDebug() << "ERROR : All sockets connected(4) with pending connection.";
    }
}
void TcpPort::setRecieveDataMode()
{
    qDebug() << "setRecieveDataMode()";
    recieved_data1 = new QByteArray;
    recieved_data2 = new QByteArray;
    recieved_data3 = new QByteArray;
    recieved_data4 = new QByteArray;

    // umtil 555 recieved read data


    socket1->flush();
    socket1->readAll();

    if(socket1->state() == QAbstractSocket::ConnectedState){
        connect(this->socket1,SIGNAL(readyRead()),this,SLOT(readDATA()));




        this->recieve_mode = true;
    }
    if(socket2->state() == QAbstractSocket::ConnectedState){
        connect(this->socket2,SIGNAL(readyRead()),this,SLOT(readDATA()));
        this->recieve_mode = true;
    }
    if(socket3->state() == QAbstractSocket::ConnectedState){
        connect(this->socket3,SIGNAL(readyRead()),this,SLOT(readDATA()));
        this->recieve_mode = true;
    }
    if(socket4->state() == QAbstractSocket::ConnectedState){
        connect(this->socket4,SIGNAL(readyRead()),this,SLOT(readDATA()));
        this->recieve_mode = true;
    }
    if(this->recieve_mode == false){
        qDebug() << "ERROR : All sockets disconnected(4), could not ender recive data mode.";
    }

}

void TcpPort::exitRecieveDataMode() // should be specific for each socket
{
    if(socket1->bytesAvailable() > 0 && socket2->bytesAvailable() > 0 && socket3->bytesAvailable() > 0 && socket4->bytesAvailable() > 0){   // all sockets not empty
        qDebug() << "Data remaining in some socket, could not exid DR mode";
        return;
    }
    //socket1->flush();
    //socket1->readAll();

    if(socket1->state() == QAbstractSocket::ConnectedState){
        disconnect(this->socket1,SIGNAL(readyRead()),this,SLOT(readDATA()));
        this->recieve_mode = false;
    }
    if(socket2->state() == QAbstractSocket::ConnectedState){
        disconnect(this->socket2,SIGNAL(readyRead()),this,SLOT(readDATA()));
        this->recieve_mode = false;
    }
    if(socket3->state() == QAbstractSocket::ConnectedState){
        disconnect(this->socket3,SIGNAL(readyRead()),this,SLOT(readDATA()));
        this->recieve_mode = false;
    }
    if(socket4->state() == QAbstractSocket::ConnectedState){
        disconnect(this->socket4,SIGNAL(readyRead()),this,SLOT(readDATA()));
        this->recieve_mode = false;
    }
    if(this->recieve_mode == true){
        qDebug() << "ERROR : failed to disconnect all sockets";
    }
    else{
       qDebug() << "exited data mode";
    }

}
void TcpPort::readDATA() // for state // dod2lat implementaci pro multichannel , po vyreseni alokace
{
    //qDebug() << socket1->readAll();
    QByteArray EOD;
    QByteArray overflow_buffer;
    EOD[0] = 5;
    EOD[1] = 5;
    EOD[2] = 5;

    while(this->socket1->bytesAvailable() > 0){
        recieved_data1->append(this->socket1->readAll());
    }
    // zastarala funkce

    if(recieved_data1->length() >= 4096){
        /*// liche vzorky by se musely zahodit kvuli rekonstrukci
        if(recieved_data1->length()%2 == 1){ // ulozim si posledni char a dam ho do dalsiho arraye
            char c = recieved_data1->back();
            recieved_data1->resize(recieved_data1->length()-1); // musim velikost o 1 zmensit jinak lichy pocet/2 = crash

            emit outputDATAframe(recieved_data1);
            QThread::usleep(100);
            //for(int i=0;i<recieved_data1->length();i++){
            //    qDebug() << QString::number(recieved_data1->at(i));
            //}
            reallocArrays();
            recieved_data1->append(c);
        }
     */
     if(recieved_data1->length() > 4096){ // pokud se nacte vic znaku nez jeden frame
            qDebug() << "too big frame";
            qDebug() << recieved_data1->length();
            int lenght_difference = recieved_data1->length() - 4096 ;
            qDebug() << recieved_data1->length()-lenght_difference;
            for(int i=0;i<lenght_difference;i++){
                overflow_buffer.append(recieved_data1->at(4096+i)); // ulozeni pretecenych dat
            }
            recieved_data1->resize(recieved_data1->length()-lenght_difference); // musim velikost o 1 zmensit jinak lichy pocet/2 = crash
            emit outputDATAframe(recieved_data1);
            QThread::usleep(100);
            reallocArrays();
            recieved_data1->append(overflow_buffer);

        }
        else{

        emit outputDATAframe(recieved_data1);
        QThread::usleep(100); // aby se stihlo vyprazdni pole nez se dealokuje
        reallocArrays(); // deletes data that has been send of, maybe different for multichannel
        }
    }
    else if(recieved_data1->contains(EOD)){
        qDebug() << "EOD found. everything ok";
        qDebug() << recieved_data1->length();
        exitRecieveDataMode();
        emit outputDATA(recieved_data1);
        emit endOfDATA();
    }
}


