#ifndef DATAANALYZER_H
#define DATAANALYZER_H

#include <QObject>
#include <QDebug>
#include <fftw3.h>
#include<cmath>
/* NAVRH FUNKCE SPK ANALYZY
 * 1 - port to navzorkuje a posle do databaze
 * 2 - rekonstrukce v databazi
 * 3 - databaze to ulozi a posle fftanalyzeru
 * 4 - udela se fft framu a posle se to ui
 *
 *  FUNKCE Z PAMETI
 * 1 - databaze loadne soubor a posle vsechny data analyzeru
 * 2 - specialni funkce naseka data a nakrmi je do fft analyzy z normalniho realtime postupu
 * 3 - posle se to ui najednou do specialni funkce ktera zobrazi celej spk hned
 *
 *
 *
 * TO OD:
 * 1. spojeni dataanalyzeru a spectrogramuu pomoci pointeru, neni to moc expandibilni pro vic kanalu - chce to jiny pristup
*/

class DataAnalyzer : public QObject
{
    Q_OBJECT
public:
    explicit DataAnalyzer(QObject * parent = nullptr, QObject * database = nullptr, QObject * tcpport= nullptr,QObject * spectrogram_1= nullptr);
    int getFrameCount();

    double * fft(uint16_t *in, int frame_lenght);
    uint16_t * hann_window(uint16_t * dataIn, int frame_lenght);
    double *oneSideSpectrum(double * two_sided_spectrum, int frame_lenght); // crops spectrum above fs/2


    uint16_t * fftExampleData(); // sinusssssssssssssssssssss
private:
    QObject * main_window;
    QObject * database;
    QObject * tcp_port;
    QObject * spectrogram_1;

    uint16_t * data;
    double pi;
    int frame_counter;

    void connectSingnals();
signals:
    void updateSpectrogram(double * spectrum_line,int frame_lenght,int frame_count);

public slots:
     //void analyzeData(uint16_t * loadData);
    void initRealTimeFFT();
    void deInitRealTimeFFT();

    void realTimeFrameFFT(uint16_t *in, int frame_lenght);
    void memoryDataFFT(); // pro nacitani ze souboru
};

#endif // DATAANALYZER_H
