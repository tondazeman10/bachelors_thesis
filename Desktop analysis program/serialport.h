#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QVariant>


/*
 CMD's for ESP:
 1 - ACK
 2 - incorrect input
 3 - measure (start measure sequence)
*/
/* every command expects ack messaage
*/

class SerialPort : public QSerialPort
{
    Q_OBJECT
public:
    void setPortName(QString name);
    void setBaudRate(int baudrate);

    // CMD's for ESP
    void cmdMeasure(char duration);
    void sendCMD(char c);
    char readCMD();

    explicit SerialPort(QObject *parent = nullptr);
    void fillPorts();

private:
    QObject * mainwindow;
    QSerialPort * qserialport;
    QString port_name = "";
    QByteArray recieved_data;
    int baud_rate = 0;
    bool connected;

    // serial port connection state
    bool getConnectionState();
    void changeConnectionState(bool state);

    // port mode - default cmd mode
    void setRecieveDataMode();
    void exitRecieveDataMode();
signals:
    QString getBaudrate();
    QString getPortNuber();
    void incrementID();
    void hideSerialDialog();

    void displayTextCOM();

public slots:
    void openPort();

    //void sendCMD();

    void readDATA();
};

#endif // SERIALPORT_H
