#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>

#include "serialport.h"
#include "tcpport.h"

class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = nullptr, SerialPort * serialport = nullptr, TcpPort * tcpport = nullptr);

    void setupDir();
    void makeTXT(QString file_name);
    void makeCSV(QString file_name);

    void writeTXTdata(QString file_name, QString data);
    void writeCSVdata(QString file_name, QByteArray data);

    QString makeID();

    QString readTXTdata();
    QByteArray writeTXTdata();

private:
    int subjectCount;
    QObject * main_window;
    SerialPort * serialport;
    TcpPort * tcpport;
    QString currentID;

    QDir * dir;
    QFile * file;

    void recunstructData(QByteArray * input_data);
    void saveDataToFile(QString file_name, uint16_t * dataArray,int samples);

signals:
    void deallock();

    void initRealTimeFFT();
    void deInitRealTimeFFT();

    void calculateFrameFFT(uint16_t * dataIn,int frame_lenght);

    // nasledne implementovat fft ze souboru


public slots:
    void incrementSubjectCount();
    void inputDATA(QByteArray * input_array);
    void inputDATAframe(QByteArray * input_array);
    void endOfInputDATA();

};

#endif // DATABASE_H
