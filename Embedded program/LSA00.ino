#include <WiFi.h>
#include <WiFiAP.h>
#include "SPI.h"

const char* ssid = "UDPtest";
const char* password =  "1231331321";

const uint16_t port = 1234;
const char * host = "192.168.4.2";

WiFiClient client;

// Define ALTERNATE_PINS to use non-standard GPIO pins for SPI bus

#ifdef ALTERNATE_PINS
  #define VSPI_MISO   2
  #define VSPI_MOSI   4
  #define VSPI_SCLK   0
  #define VSPI_SS     33

  #define HSPI_MISO   26
  #define HSPI_MOSI   27
  #define HSPI_SCLK   25
  #define HSPI_SS     32
#else
  #define VSPI_MISO   MISO
  #define VSPI_MOSI   MOSI
  #define VSPI_SCLK   SCK
  #define VSPI_SS     21//SS

  #define HSPI_MISO   12
  #define HSPI_MOSI   13
  #define HSPI_SCLK   14
  #define HSPI_SS     15
#endif

static const int spiClk = 25000000; // 25 MHz
//uninitalised pointers to SPI objects
SPIClass * vspi = NULL;
SPIClass * hspi = NULL;

// definice pro prenos dat 
#define fs 16129
#define frame_lenght 2048
#define channels 1
//#define ANALOG_PIN 15

//int channels = 1; // 1,2,4,6,8
volatile int s_counter = 0;
volatile int f_counter = 0;
volatile bool timer_en = 0;

bool bufFlag[2] = {0,0};
uint8_t buffer_0[channels * 2][frame_lenght*2]; // every channel has 2 rows as buffers for samples
volatile short buffer_0_flag = 2; // 0 - filling first row, 1 - filling second row empty, 2 -idle

// definition of multicore tasks core 1 is default, core 0 secondary
TaskHandle_t core0Task;
hw_timer_t * timer = NULL; // pointer to timer object .
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED; // variable for protection of timer triggerfunction

union TESTDATA {  // definice unionu pro trhani dat na 2 byty
  uint16_t data_01;
  struct {
    uint8_t data_0;
    uint8_t data_1;
  };
  };
  union TESTDATA uint16;

// --------------------  WiFi -------------------------  

void setupWifi(){
  Serial.println();
  Serial.println("Configuring access point...");
  WiFi.softAP(ssid, password);
  
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  //server.begin();

  Serial.println("Server started");
  connectTcp();
  }
  
void connectTcp(){
  if (!client.connect(host, port)) {
        Serial.println("Connection to host failed");
        delay(1000);
        return;
    }
    Serial.println("Tcp socket connected!");
  }
  

void disconnectTcp(){
  client.stop();
  }
void sendEOD(){
  client.write(5);
  client.write(5);
  client.write(5);
  client.flush();
  }
// --------------------  SPI -------------------------  

//ADC INFO --------------

// t_aq = cas pro nabiti mericiho kondenzatoru 
//   min t_aq = 700ns
// t_c = Data Conversion Time:
// t_c = 1300ns; 
// f_spi_min = 23.53MHz pro 500ksps
//nema vstupni pin pro data, pripojen na vcc 

// pokud je cs high (norm stav) konvertuje adc namereny vstup(min 1300ns)
// pote co cs=0 odesle adc data a meri na vstupu (min 700ns), lepsi merit az po odeslani dat
// goto bod 1

void setupSPI(){
  Serial.println();
  Serial.println("Setting up SPI");

    //initialise vspi object
  vspi = new SPIClass(VSPI);
  
  SPI.begin();

  //set up slave select pins as outputs as the Arduino API
  //doesn't handle automatically pulling SS low
  pinMode(VSPI_SS, OUTPUT); //VSPI SS
  //pinMode(HSPI_SS, OUTPUT); //HSPI SS
  digitalWrite(VSPI_SS, LOW); //pull cs down to send data, stay low as normal state 
  }
uint16_t vspiListen() { // get one sample
  uint16_t sample;
  SPI.beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE1));
  digitalWrite(VSPI_SS, HIGH); // start converison 
  delayMicroseconds(2); // for higher samplerate make delas shorter, makes sure conv finishes
  digitalWrite(VSPI_SS, LOW); //pull cs down to send data, stay low as normal state 
  delayMicroseconds(2); // delay to make sure all data comes in
  sample = SPI.transfer16(0);  
  SPI.endTransaction();
  return sample;
}
// --------------------  Komunikace  -------------------------  
void waitForCMD(){
  byte cmd; // cmd placeholder
  while(1){ // stuck in loop intil serial or tcp input
    if(Serial.available()){
        Serial.readBytes(&cmd, 1);
        break;
      }
    else if(client.available()){
        client.readBytes(&cmd, 1);
        break;
      }
    else if(!client.connected())
      connectTcp();
    }  
    switch(cmd) { //  task logic , pop first byte of cmd 
  case 0: // idle CMD
    break;
    
  case 1: // ack cmd 
    //Serial.write(1); // ack
    client.write(1); // ack
    client.flush();
    cmd = 0; // nulling cmd
      break;
      
   case 2: // incorrect msg 
      //Serial.write(1); // ack
      client.write(1); // ack
      client.flush();
      cmd = 0; // nulling cmd
      break;
      
   case 3: // measure sequence 
      //Serial.write(1); // ack
      //client.write(1); // ack
      //client.flush();
        // sekvence ceka na dalsi 2 byty pro urceni delky mereni
      //delayMicroseconds(100);
      byte lenght;
      while(!client.available()){}
      client.read(&lenght, 1);
      //Serial.print(lenght);
      measure(lenght); // lenght is mesurement argument in seconds  // 555 
      cmd = 0; // nulling cmd
      break;
      
  default:
      cmd = 0; // nulling cmd
    break;
  } 
}
void IRAM_ATTR onTimer() { // rozdelovani a spojovani cisel funguje suprove 
  portENTER_CRITICAL_ISR(&timerMux);
  if(timer_en == 1){
    for(int i=0;i<channels*2;i=i+2){  //  takes sample for every channel
      uint16.data_01 = vspiListen();
      buffer_0[i+buffer_0_flag][s_counter*2] = uint16.data_0; // prvnich 8 bitu
      buffer_0[i+buffer_0_flag][s_counter*2+1] = uint16.data_1; // poslednich 8 bitu
      //buffer_0[i+buffer_0_flag][s_counter] = vspiListen();
    }
    s_counter++;
  }
  portEXIT_CRITICAL_ISR(&timerMux); 
  if(s_counter >= frame_lenght){ // pokud je namereno dostatek vzorku na jeden segmet tak se preda signal na ulozeni a odeslani dat 
  s_counter = 0;
  f_counter++;
  bufFlag[buffer_0_flag] = 1; // 1 indikuje ze je buffer ready na odeslani ulozeni a vymazani 
  buffer_0_flag =f_counter % 2;
  }
}

void setupTimer(){
  Serial.println("Setting up timer");
  timer = timerBegin(0, 80, true); // myslim ze je to prescaler  - f = 240 MHz -> 240  timer = timerBegin(0, 80, true); 80 plati pro esp8266
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 62, true);
  timerWrite(timer, 0);
  timerAlarmEnable(timer);  
}

void measure(byte lenght){
  // wait for timer to reset
  // prepare counting variables
  // prepare second core for sending 
  int frms = lenght * 8;
  timer_en = 1; // enables onTimer function to take samples 
  f_counter = 0;
  buffer_0_flag = 0;
  while(f_counter<frms){
  }
  timer_en = 0; // disables onTimer function to take samples
  buffer_0_flag = 2;
  delay(300); // to make sure all frames are sent before EOD
  sendEOD();
}
void nullBuffer(){
  for(int i=0;i<2;i++){
    for(int j=0;j<frame_lenght;j++){
        buffer_0[i][j] = 0; 
      }
    }
     Serial.println("Buffer nulled");
}
void nullRow(int k){
    for(int j=0;j<frame_lenght;j++){
        buffer_0[k][j] = 0; 
      }
}
void printBuffer(){
  Serial.println("");
  for(int i=0;i<channels*2;i++){
    for(int j=0;j<frame_lenght;j++){
      Serial.print(buffer_0[i][j]);
      }
     Serial.println("");
    }
   Serial.println("Buffer printed");
}
void setupTask(){
  xTaskCreatePinnedToCore(coreTask,"CodeForCore0" ,10000 ,NULL ,1 ,&core0Task ,0); // pin send buffer to second core
  disableCore0WDT(); // disable second core task timer watchdog 
  }
// --------------------  Program  -------------------------  

void setup() {
  Serial.begin(115200);
  setupWifi(); 
  //connectTcp();
  setupSPI();
  setupTask();
  setupTimer();
  nullBuffer();
  //printBuffer();
  
}

void loop() {
  waitForCMD();
}
// --------------------  Program - second core  -------------------------  

void coreTask(void * pvParameters){ // proces bezici na druhem jadru - kdyz se zmeni buffer flag tak vypise buffer pro vsechny kanaly
  while(1){
    if(bufFlag[0] == 1){ // pokud probiha vzorkovani probiha vickrat 
      //Serial.println(Serial.availableForWrite());
        for(int i = 0;i<channels*2;i=i+2){  //  takes sample for every channel
          //Serial.write(buffer_0[i],frame_lenght*2);
          //Serial.println("");
          client.write(buffer_0[i],frame_lenght*2);
          client.flush();  
          //nullRow(i);    
        }
        //Serial.println("bb");
        bufFlag[0] = 0;
    }
    else if(bufFlag[1] == 1){ // pokud probiha vzorkovani probiha vickrat 
        for(int i = 1;i<channels*2;i=i+2){  //  takes sample for every channel
          //Serial.write(buffer_0[i],frame_lenght*2);
          //Serial.println(""); 
          client.write(buffer_0[i],frame_lenght*2);
          client.flush();
          //nullRow(i);         
        }
        //Serial.println("bb");
        bufFlag[1] = 0;
    }
  } 
}//ff
