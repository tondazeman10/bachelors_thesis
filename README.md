Author: 	Antonin Zeman
Date: 		26.1.2022
Date of completion: 6.2019

Description: This bachelor thesis deals with the development of an analyzer which can be used to diagnose asthma. The principle of the used analysis consists of the detection of specific wheezes in lung sounds. The thesis begins with a description of methods that are used to diagnose asthma in the Czech Republic. The last part of the theoretical introduction is an overview of acoustic sensors suitable for this application. A condenser microphone was chosen as the most suitable type. The frequency band in the range of 100-3200 Hz was monitored. The result of the analysis is displayed in the form of a spectrogram. The functionality of the proposed analyzer was verified by detecting wheezes in a patient suffering from asthma.	

Conclusion: The proposed device was completed in form of a prototype. When using this prototype asthma wheeses where succesfully acquired.


Notes:

	The practical part of this thesis consists of three different parts:
	
		1, The physical design of pcb of proposed prototype.
		
		2, Program of embedded system.
		
		3, Desktop program for data analysis and spectrogram construction.

